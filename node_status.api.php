<?php

/**
 * @file
 * Hook_node_status_block_enable_flag.
 * @param $fid: Flag ID
 * @param $node: Node
 *
 * @return True or False, based on whether you want to enable the flag for the current node state. *  */

/**
 *
 */
function hook_node_status_enable_flag($node, $fid) {
  return TRUE;
}
